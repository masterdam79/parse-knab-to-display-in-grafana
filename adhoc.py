#!/usr/bin/env python3

from csv import reader
import configparser
from influxdb import InfluxDBClient
import argparse
import json
import datetime


parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--value', type=float, required=True)
parser.add_argument('--tag', type=str, required=True)
parser.add_argument('--date', type=str, required=True)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    influx_host = config['DETAILS']['INFLUXDB_URL']
    db_name = config['DETAILS']['DB_NAME']
    measurement = config['DETAILS']['MEASUREMENT']


client = InfluxDBClient(host=influx_host, port=8086)
client.create_database(db_name)
client.get_list_database()
client.switch_database(db_name)

date = args.date

print(date)

formatted_date = datetime.datetime.strptime(date, '%d-%m-%Y').strftime('%Y-%m-%d')

print(formatted_date)

json_body = [
    {
        "measurement": 'adhoc',
        "tags": {
            "tag": args.tag
        },
        "time": formatted_date + "T0:00:00Z",
        "fields": {
            "value": args.value
        }
    }
]

jsonStr = json.dumps(json_body)
print(jsonStr)
client.write_points(json_body)
